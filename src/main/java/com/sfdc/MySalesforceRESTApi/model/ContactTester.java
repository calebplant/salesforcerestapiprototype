package com.sfdc.MySalesforceRESTApi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactTester {
    @JsonProperty(value="Id")
    String id;
    @JsonProperty(value = "Name")
    String name;
    @JsonProperty(value = "Account")
    AccountTester account;

    public AccountTester getAccount() {
        return account;
    }

    public void setAccount(AccountTester account) {
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
