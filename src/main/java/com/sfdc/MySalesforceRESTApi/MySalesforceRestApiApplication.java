package com.sfdc.MySalesforceRESTApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySalesforceRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySalesforceRestApiApplication.class, args);
	}

}
