package com.sfdc.MySalesforceRESTApi.service;

import com.force.api.ApiConfig;
import com.force.api.ForceApi;
import com.sfdc.MySalesforceRESTApi.model.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Service
public class ApiService {
    @Value("${authenticationProperties.username}")
    private String username;
    @Value("${authenticationProperties.password}")
    private String password;
    @Value("${authenticationProperties.securityToken}")
    private String securityToken;
    @Value("${authenticationProperties.consumerSecret}")
    private String consumerSecret;
    @Value("${authenticationProperties.consumerKey}")
    private String consumerKey;

    public ForceApi api;

    @PostConstruct
    private void initializeApi()
    {
        this.api = new ForceApi(new ApiConfig()
            .setUsername(username)
            .setPassword(password+securityToken)
            .setClientId(consumerKey)
            .setClientSecret(consumerSecret));
        System.out.println(api.getSession());
    }

//    public AuthenticationResponse login(){
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        MultiValueMap<String, String> params= new LinkedMultiValueMap<String, String>();
//
//        params.add("username", username);
//        params.add("password", password+securityToken );
//        params.add("client_secret", consumerSecret );
//        params.add("client_id", consumerKey);
//        params.add("grant_type","password");
//
//        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(params, headers);
//
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity response = restTemplate.postForEntity("https://login.salesforce.com/services/oauth2/token", request, AuthenticationResponse.class);
//        return (AuthenticationResponse) response.getBody();
//    }

    public ForceApi login() {
        ForceApi api = new ForceApi(new ApiConfig()
                .setUsername(username)
                .setPassword(password+securityToken)
                .setClientId(consumerKey)
                .setClientSecret(consumerSecret));
        System.out.println(api);
        return api;
    }
}
