package com.sfdc.MySalesforceRESTApi.service;

import com.sfdc.MySalesforceRESTApi.model.AccountResponse;
import com.sfdc.MySalesforceRESTApi.model.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class MySalesforceRestApiService {

    @Value("${authenticationProperties.username}")
    private String username;
    @Value("${authenticationProperties.password}")
    private String password;
    @Value("${authenticationProperties.securityToken}")
    private String securityToken;
    @Value("${authenticationProperties.consumerSecret}")
    private String consumerSecret;
    @Value("${authenticationProperties.consumerKey}")
    private String consumerKey;

    public AuthenticationResponse login(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> params= new LinkedMultiValueMap<String, String>();

        params.add("username", username);
        params.add("password", password+securityToken );
        params.add("client_secret", consumerSecret );
        params.add("client_id", consumerKey);
        params.add("grant_type","password");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(params, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity response = restTemplate.postForEntity("https://login.salesforce.com/services/oauth2/token", request, AuthenticationResponse.class);
        return (AuthenticationResponse) response.getBody();
    }

    public AccountResponse getAccountData(String accessToken, String instanceUrl){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + accessToken);
        MultiValueMap<String, String> params= new LinkedMultiValueMap<String, String>();

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(params, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity salesforceTestData = restTemplate.exchange(instanceUrl+ "/services/data/v48.0/query?q=select Id, Name from Account", HttpMethod.GET, request, AccountResponse.class);
        return (AccountResponse) salesforceTestData.getBody();
    }
}
