package com.sfdc.MySalesforceRESTApi.controller;

import com.force.api.QueryResult;
import com.sfdc.MySalesforceRESTApi.model.AccountResponse;
import com.sfdc.MySalesforceRESTApi.model.AccountTester;
import com.sfdc.MySalesforceRESTApi.model.AuthenticationResponse;
import com.sfdc.MySalesforceRESTApi.model.ContactTester;
import com.sfdc.MySalesforceRESTApi.service.ApiService;
import com.sfdc.MySalesforceRESTApi.service.MySalesforceRestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MySalesforceRESTApiController {

    @Autowired
    private MySalesforceRestApiService mySalesforceRestApiService;

    @Autowired
    private ApiService apiService;

    @RequestMapping("/home")
    public String getSalesforceObject()
    {
        return "home";
    }

    @GetMapping("/bananas")
    public String getBananas() {
        QueryResult<AccountTester> res = apiService.api.query("SELECT Id, Name FROM Account", AccountTester.class);
        System.out.println(res);
        return String.valueOf(res);
    }

    @GetMapping("/contacts")
    public String getContacts() {
        QueryResult<ContactTester> res = apiService.api.query("SELECT Id, Name, Account.Name FROM Contact", ContactTester.class);
        System.out.println(res);
        return String.valueOf(res);
    }

    @RequestMapping("/account")
    public AccountResponse getSalesforceAccount(){
        AuthenticationResponse authenticationResponse = mySalesforceRestApiService.login();
        System.out.println(authenticationResponse);
        AccountResponse accountResponse = mySalesforceRestApiService.getAccountData(authenticationResponse.getAccess_token(), authenticationResponse.getInstance_url());
        return accountResponse;
    }
}
